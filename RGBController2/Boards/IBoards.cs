﻿using System;

namespace RGBController2.Boards
{
    /// <summary>
    /// Interface for LED devices.
    /// </summary>
    public interface IBoard
    {
        /// <summary>
        /// The connection status of the device.
        /// </summary>
        public bool Connected { get; }

        /// <summary>
        /// Sets all of the LEDs to the chosen RGB colour.
        /// </summary>
        /// <param name="red">The red value</param>
        /// <param name="green">The green value</param>
        /// <param name="blue">The blue value</param>
        public void SetAllLeds(byte red, byte green, byte blue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Turns off all of the LEDs connected to the board. 
        /// </summary>
        public void TurnOffAllLeds()
        {
            throw new NotImplementedException();
        }
    }
}
