﻿using CUE.NET;
using CUE.NET.Brushes;
using CUE.NET.Devices.Generic.Enums;
using CUE.NET.Exceptions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using CUE.NET.Devices.Generic;

namespace RGBController2.Boards
{
    /// <summary>
    /// CUEDeviceBoard used for communicating with a single CUE device.
    /// </summary>
    public class CUEDeviceBoard : IBoard
    {
        /// <summary>
        /// The corsair CUE device to control.
        /// </summary>
        private AbstractCueDevice _device;

        /// <summary>
        /// The type of CUE device. i.e mouse or keyboard.
        /// </summary>
        private CorsairDeviceType _deviceType;
        public CorsairDeviceType DeviceType
        {
            get { return _deviceType; }
            set { _deviceType = value; }
        }

        /// <summary>
        /// An array of available CUE devices.
        /// </summary>
        private CorsairDeviceType[] _availableDevices;
        public CorsairDeviceType[] AvailableDevices
        {
            get { return _availableDevices;  }
        }

        /// <summary>
        /// The connectionstatus of the CUE device.
        /// </summary>
        private bool _connected;
        public bool Connected
        {
            get { return _connected; }
        }

        /// <summary>
        /// Contructs oject to control Cue devices. 
        /// Note: this does not connect to a device.
        /// </summary>
        public CUEDeviceBoard()
        {
            List<CorsairDeviceType> device = new List<CorsairDeviceType>();
            // Find which devices are available
            if (CueSDK.IsSDKAvailable(CorsairDeviceType.Headset))
                device.Add(CorsairDeviceType.Headset);
            if (CueSDK.IsSDKAvailable(CorsairDeviceType.HeadsetStand))
                device.Add(CorsairDeviceType.HeadsetStand);
            if (CueSDK.IsSDKAvailable(CorsairDeviceType.Keyboard))
                device.Add(CorsairDeviceType.Keyboard);
            if (CueSDK.IsSDKAvailable(CorsairDeviceType.Mouse))
                device.Add(CorsairDeviceType.Mouse);
            if (CueSDK.IsSDKAvailable(CorsairDeviceType.Mousemat))
                device.Add(CorsairDeviceType.Mousemat);

            _availableDevices = device.ToArray();
            _connected = false;
        }

        /// <summary>
        /// Connects to and initilises the given device.
        /// </summary>
        /// <param name="deviceType">The type of device to connect to</param>
        public void ConnectToDevice(CorsairDeviceType deviceType)
        {
            try
            {
                CueSDK.Initialize(true);
                switch(deviceType)
                {
                    case CorsairDeviceType.Headset:
                        _device = CueSDK.HeadsetSDK;
                        break;
                    case CorsairDeviceType.HeadsetStand:
                        _device = CueSDK.HeadsetStandSDK;
                        break;
                    case CorsairDeviceType.Keyboard:
                        _device = CueSDK.KeyboardSDK;
                        break;
                    case CorsairDeviceType.Mouse:
                        _device = CueSDK.MouseSDK;
                        break;
                    case CorsairDeviceType.Mousemat:
                        _device = CueSDK.MousematSDK;
                        break;
                }

                if (_device == null)
                    throw new WrapperException("No CUE device found");
                _device.Brush = (CUE.NET.Brushes.SolidColorBrush)Color.Transparent; // This 'enables' manual color changes.

                // Sets the device to blue, this should probally be changed to a different colour
                SolidColorBrush solidColorBrush = new SolidColorBrush(Color.Blue);
                _device.Brush = solidColorBrush;
                _device.Update();
                _connected = true;
            }
            catch (CUEException ex)
            {
                MessageBox.Show("CUE Exception! ErrorCode: " + Enum.GetName(typeof(CorsairError), ex.Error));
            }
            catch (WrapperException ex)
            {
                    MessageBox.Show("Wrapper Exception! Message:" + ex.Message);
            }
        }

        /// <summary>
        /// Sets all of the LEDs to the chosen RGB colour.
        /// </summary>
        /// <param name="red">The red value</param>
        /// <param name="green">The green value</param>
        /// <param name="blue">The blue value</param>
        public void SetAllLeds(byte red, byte green, byte blue)
        {
            if (Connected)
            {
                SolidColorBrush solidColorBrush = new SolidColorBrush(Color.FromArgb(255, (byte)red, (byte)green, (byte)blue));
                _device.Brush = solidColorBrush;
                _device.Update();
            }
        }

        /// <summary>
        /// Turns off all of the LEDs for that device. 
        /// </summary>
        public void TurnOffAllLeds()
        {
            if (Connected)
            {
                SolidColorBrush solidColorBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
                _device.Brush = solidColorBrush;
                _device.Update();
            }
        }

    }
}
