﻿using Corale.Colore.Core;
using System;
using System.Collections.Generic;
using System.Text;
using ColoreColor = Corale.Colore.Core.Color;

namespace RGBController2.Boards
{
    public class ChromaDeviceBoard : IBoard
    {
        /// <summary>
        /// True is communication has been established with the Arduino, false otherwise.
        /// </summary>
        public bool Connected { get; }

        public enum DeviceTypes
        {
            Mousepad,
            Keypad,
            Headset,
            Keyboard,
            Mouse
        }

        private DeviceTypes _deviceType;

        /// <summary>
        /// Constructs a ChromaDeviceBoard for controlling a Chroma device.
        /// </summary>
        /// <param name="deviceType">The type of the chroma device to control.</param>
        public ChromaDeviceBoard(DeviceTypes deviceType)
        {
            Connected = true;
            _deviceType = deviceType;
            // This is here because the device needs to be initilised by the first call
            // This won't actually set the colour
            SetAllLeds(255, 255, 255);
        }

        /// <summary>
        /// Sets all of the LEDs to the chosen RGB colour.
        /// </summary>
        /// <param name="red">The red value</param>
        /// <param name="green">The green value</param>
        /// <param name="blue">The blue value</param>
        public void SetAllLeds(byte red, byte green, byte blue)
        {
            var colour = new ColoreColor((byte)red, (byte)green, (byte)blue);

            switch (_deviceType)
            {
                case DeviceTypes.Mousepad:
                    Chroma.Instance.Mousepad.SetAll(colour);
                    break;
                case DeviceTypes.Mouse:
                    Chroma.Instance.Mouse.SetAll(colour);
                    break;
                case DeviceTypes.Keypad:
                    Chroma.Instance.Keypad.SetAll(colour);
                    break;
                case DeviceTypes.Keyboard:
                    Chroma.Instance.Keyboard.SetAll(colour);
                    break;
                case DeviceTypes.Headset:
                    Chroma.Instance.Headset.SetAll(colour);
                    break;
            }
        }
    }
}
