﻿using RGBController2.ViewModels;
using System;
using System.Windows;

namespace RGBController2.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Windows.Forms.NotifyIcon _notifyIcon;
        public MainWindow()
        {
            InitializeComponent();
            // This allows the biniding to be done to the MainViewModel.cs class rather than this one.
            // Why? Something to do with MVVM & good practice?
            var viewModel = new MainViewModel();
            DataContext = viewModel;

            // We are using this to hook on the closing event so that we can save our config when the application is closed
            Closing += viewModel.OnWindowClosing;
            
            // System tray code
            // This should all probally be in the view model, but then I cannot
            // access the window state and this object
            _notifyIcon = new System.Windows.Forms.NotifyIcon();
            _notifyIcon.Icon = new System.Drawing.Icon("logo.ico");
            _notifyIcon.MouseDoubleClick +=
                new System.Windows.Forms.MouseEventHandler
                    (NotifyIcon_MouseDoubleClick);
            this.StateChanged += this.Window_StateChanged;

            // Start minimised
            WindowState = WindowState.Minimized;
            this.ShowInTaskbar = false;
            _notifyIcon.Visible = true;
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                _notifyIcon.Visible = true;
            }
            else if (this.WindowState == WindowState.Normal)
            {
                _notifyIcon.Visible = false;
                this.ShowInTaskbar = true;
            }
        }

        private void NotifyIcon_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }
    }
}
