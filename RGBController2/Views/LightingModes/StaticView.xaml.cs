﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RGBController2.ViewModels.LightingModes;
using RGBController2.ViewModels;
using System.Collections.ObjectModel;
using System.Linq;
using RGBController2.ViewModels.Tabs;

namespace RGBController2.Views.LightingModes
{
    /// <summary>
    /// Interaction logic for StaticView.xaml
    /// </summary>
    public partial class StaticView : UserControl
    {
        public StaticView()
        {
            InitializeComponent();
        }
    }
}
