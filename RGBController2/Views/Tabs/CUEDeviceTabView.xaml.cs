﻿using RGBController2.ViewModels.Tabs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RGBController2.Views.Tabs
{
    /// <summary>
    /// Interaction logic for ArduinoTabView.xaml
    /// </summary>
    public partial class CUEDeviceTabView : UserControl
    {
        public CUEDeviceTabView()
        {
            InitializeComponent();
        }
    }
}
