﻿using RGBController2.Commands;
using System.Windows;
using System.Windows.Input;
using RGBController2.Boards;
using System.Windows.Media;

namespace RGBController2.ViewModels.LightingModes
{
    /// <summary>
    /// The view model for the StaticView.
    /// </summary>
    public class StaticViewModel : BaseViewModel
    {
        /// <summary>
        /// The Numericelector value for the red colour.
        /// </summary>
        private int _redColourSend;
        public int RedColourSend
        {
            get { return _redColourSend; }
            set 
            { 
                _redColourSend = value;
                UpdateSendColour();
            }
        }

        /// <summary>
        /// The Numericelector value for the green colour.
        /// </summary>
        private int _greenColourSend;
        public int GreenColourSend
        {
            get { return _greenColourSend; }
            set 
            { 
                _greenColourSend = value;
                UpdateSendColour();
            }
        }

        /// <summary>
        /// The Numericelector value for the blue colour.
        /// </summary>
        private int _blueColourSend;
        public int BlueColourSend
        {
            get { return _blueColourSend; }
            set 
            { 
                _blueColourSend = value;
                UpdateSendColour();
            }
        }

        /// <summary>
        /// The label value for the red current colour.
        /// </summary>
        private int _redColourCurrent;
        public int RedColourCurrent
        {
            get { return _redColourCurrent; }
            set { _redColourCurrent = value; }
        }

        /// <summary>
        /// The label value for the blue current colour.
        /// </summary>
        private int _blueColourCurrent;
        public int BlueColourCurrent
        {
            get { return _blueColourCurrent; }
            set { _blueColourCurrent = value; }
        }

        /// <summary>
        /// The label value for the green current colour.
        /// </summary>
        private int _greenColourCurrent;
        public int GreenColourCurrent
        {
            get { return _greenColourCurrent; }
            set { _greenColourCurrent = value; }
        }

        /// <summary>
        /// The colour used for the current colour box.
        /// </summary>
        private SolidColorBrush _colourCurrent;
        public SolidColorBrush ColourCurrent
        {
            get { return _colourCurrent; }
            set { _colourCurrent = value; }
        }

        /// <summary>
        /// The colour used for the send colour box.
        /// </summary>
        private SolidColorBrush _colourSend;
        public SolidColorBrush ColourSend
        {
            get { return _colourSend; }
            set { _colourSend = value; }
        }

        /// <summary>
        /// The command used for the send button.
        /// </summary>
        public ICommand SendCommand { get; set; }

        /// <summary>
        /// The board which the class instance will control.
        /// </summary>
        private IBoard _ledBoard;

        /// <summary>
        /// Constructor for the StaticViewModel.
        /// </summary>
        /// <param name="ledBoard">The led device which this StaticView will control.</param>
        public StaticViewModel(IBoard ledBoard)
        {
            SendCommand = new ActionCommand(o => SendButtonClick("SenderButton"));

            // Default values
            RedColourSend = 0;
            GreenColourSend = 255;
            BlueColourSend = 255;
            RedColourCurrent = RedColourSend;
            GreenColourCurrent = GreenColourSend;
            BlueColourCurrent = BlueColourSend;
            ColourCurrent = new SolidColorBrush(Color.FromRgb((byte)RedColourSend, (byte)GreenColourSend, (byte)BlueColourSend));
            ColourSend = new SolidColorBrush(Color.FromRgb((byte)RedColourSend, (byte)GreenColourSend, (byte)BlueColourSend));
            // This is here to allow us to interface with the board stored in the MainViewModel
            // ideally this would involved pointers but C# does not seem to like pointers
            _ledBoard = ledBoard;

            // ToDo load from previous values
        }

        /// <summary>
        /// Helper function used to update the colour of the send colour box.
        /// </summary>
        private void UpdateSendColour()
        {
            ColourSend = new SolidColorBrush(Color.FromRgb((byte)RedColourSend, (byte)GreenColourSend, (byte)BlueColourSend));
            OnPropertyChanged(nameof(ColourSend));
        }

        /// <summary>
        /// The call back function for the send button.
        /// </summary>
        /// <param name="sender"></param>
        private void SendButtonClick(object sender)
        {
            if (_ledBoard != null)
                if (_ledBoard.Connected)
                {
                    _ledBoard.SetAllLeds((byte)RedColourSend, (byte)GreenColourSend, (byte)BlueColourSend);
                    RedColourCurrent = RedColourSend;
                    GreenColourCurrent = GreenColourSend;
                    BlueColourCurrent = BlueColourSend;
                    ColourCurrent = new SolidColorBrush(Color.FromRgb((byte)RedColourSend, (byte)GreenColourSend, (byte)BlueColourSend));
                    OnPropertyChanged(nameof(RedColourCurrent));
                    OnPropertyChanged(nameof(GreenColourCurrent));
                    OnPropertyChanged(nameof(BlueColourCurrent));
                    OnPropertyChanged(nameof(ColourCurrent));
                }
                else
                    MessageBox.Show("Error - The device has disconnected");
            else
                MessageBox.Show("Error - No device is connected");
        }       
        
    }
}
