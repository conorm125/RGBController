﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace RGBController2.ViewModels
{
    /// <summary>
    /// The BaeViewModel from which all view models should inherit.
    /// </summary>
    public class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// An event used for when a property used by the UI is change by the view model.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Function to update the GUI View when a given property has been changed by the view model.
        /// </summary>
        /// <param name="propertyName">The name of the changed property.</param>
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
