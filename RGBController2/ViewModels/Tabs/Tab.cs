﻿using System;
using System.Windows.Input;
using RGBController2.Commands;
using RGBController2.Boards;
using RGBController2.ViewModels.LightingModes;

namespace RGBController2.ViewModels.Tabs
{
    /// <summary>
    /// An abstract class of a tab.
    /// </summary>
    public abstract class Tab : BaseViewModel, ITab
    {
        /// <summary>
        /// An enum of the different types of tabs there can be.
        /// Currently this is used for getting the typ0e of the tab for saving the config file.
        /// </summary>
        public enum tabType
        {
            Unknown,
            Arduino,
            CUE,
            Chroma
        }

        /// <summary>
        /// The type of this tab.
        /// </summary>
        public tabType TabType = tabType.Unknown;

        /// <summary>
        /// The currently selected lighting mode view model of the tab.
        /// </summary>
        private BaseViewModel _selectedLightingMode;
        public BaseViewModel SelectedLightingMode
        {
            get { return _selectedLightingMode; }
            set { _selectedLightingMode = value; }
        }

        /// <summary>
        /// The name used for the tab header.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The command used to close the tab.
        /// </summary>
        public ICommand CloseCommand { get; }

        /// <summary>
        /// The event handler to handel closing the tab.
        /// </summary>
        public event EventHandler CloseRequested;

        /// <summary>
        /// The connection status string used by the UI.
        /// </summary>
        private string _conntectionStatus;
        public string ConnectionStatus
        {
            get { return _conntectionStatus; }
            set { _conntectionStatus = value; }
        }

        /// <summary>
        /// THE RGB device that the tab will communicated with.
        /// </summary>
        public IBoard _device;

        /// <summary>
        /// An enum of the different types of tabs there can be.
        /// Currently this is used for getting the typ0e of the tab for saving the config file.
        /// </summary>
        protected enum LightingModes
        {
            StaticMode = 0,
            AnimationMode,
            QuakeMode
        }

        /// <summary>
        /// The selected lighting mode.
        /// This corresponds the the selected item from the drop down box.
        /// </summary>
        private LightingModes _lightingMode;
        public int LightingMode
        {
            get { return (int)_lightingMode; }
            set
            {
                if ((_lightingMode != (LightingModes)value) && (EnableSelectLightingMode))
                {
                    _lightingMode = (LightingModes)value;
                    switch (_lightingMode)
                    {
                        case LightingModes.AnimationMode:
                            SelectedLightingMode = new AnimationViewModel();
                            break;
                        case LightingModes.StaticMode:
                            SelectedLightingMode = new StaticViewModel(_device);
                            break;
                            //case LightingModes.QuakeMode:
                            //    SelectedLightingMode = new Q();
                            //    break;
                    }
                    OnPropertyChanged(nameof(SelectedLightingMode));
                }
            }
        }

        /// <summary>
        /// Binded to the enabled value of the SlectLightingMode combo box
        /// </summary>
        private bool _enableSelectLightingMode = false;
        public bool EnableSelectLightingMode
        {
            get { return _enableSelectLightingMode; }
            set { _enableSelectLightingMode = value; }
        }

        /// <summary>
        /// Creates a tab and sets up the close command.
        /// </summary>
        public Tab()
        {
            CloseCommand = new ActionCommand(p => CloseRequested?.Invoke(this, EventArgs.Empty));
        }
    }
}
