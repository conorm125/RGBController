﻿using RGBController2.ViewModels;
using System;
using System.Windows.Input;

namespace RGBController2
{
    /// <summary>
    /// An interface used to represent a tab.
    /// </summary>
    public interface ITab
    {
        /// <summary>
        /// The name used for the tab header.
        /// </summary>
        string Name { get; set;}

        /// <summary>
        /// The command used to close the tab.
        /// </summary>
        ICommand CloseCommand { get; }

        /// <summary>
        /// The event handler to handel closing the tab.
        /// </summary>
        event EventHandler CloseRequested;
    }
}
