﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using RGBController2.Boards;
using System.ComponentModel;
using RGBController2.ViewModels.LightingModes;

namespace RGBController2.ViewModels.Tabs
{
    /// <summary>
    /// The view model for an ArduinoTab.
    /// </summary>
    public class ArduinoTab : Tab, INotifyPropertyChanged
    {

        /// <summary>
        /// The array of available COM ports.
        /// This is used to populate the items in the GUI.
        /// </summary>
        private string[] _availablePorts;
        public string[] AvailablePorts
        {
            get { return _availablePorts; }
            set { _availablePorts = value; }
        }

        /// <summary>
        /// The selected port.
        /// This is the selected item from the dropdown box in the GUI.
        /// </summary>
        private string _selectedPort;
        public string SelectedPort
        {
            get { return _selectedPort; }
            set 
            {
                if (value != _selectedPort)
                {
                    _selectedPort = value;
                    // Connect to the port here
                    _device = new ArduinoBoard(_selectedPort);
                    if (_device.Connected)
                    {
                        ConnectionStatus = "Device Connected";
                        EnableSelectLightingMode = true;
                        // Set the page to static lighting mode to allow the user to change the
                        // lighting mode now that we are connected to the arduino
                        SelectedLightingMode = new StaticViewModel(_device);
                        OnPropertyChanged(nameof(SelectedLightingMode));
                    }
                    else
                    {
                        ConnectionStatus = "Failed to Connect";
                        EnableSelectLightingMode = false;
                    }
                    OnPropertyChanged(nameof(ConnectionStatus));
                    OnPropertyChanged(nameof(EnableSelectLightingMode));

                }
            }
        }

        /// <summary>
        /// Constructor for an Arduino tab.
        /// </summary>
        /// <param name="name">The tab header name.</param>
        public ArduinoTab(string name)
        {
            TabType = tabType.Arduino;
            Name = name;
            ConnectionStatus = "Device Disconnected";
            _selectedPort = "";

            // Get a list of the available com ports
            string[] ports = SerialPort.GetPortNames();
            _availablePorts = ports;

            // This is a temporary viewmodel that is used before the user has connected to a device
            SelectedLightingMode = new InformationViewModel();
        }

    }
}
