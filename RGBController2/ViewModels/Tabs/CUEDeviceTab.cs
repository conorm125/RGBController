﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using RGBController2.Boards;
using System.ComponentModel;
using RGBController2.ViewModels.LightingModes;
using CUE.NET.Devices.Generic.Enums;

namespace RGBController2.ViewModels.Tabs
{
    public class CueDeviceTab : Tab, INotifyPropertyChanged
    {
        /// <summary>
        /// An array of the available CUE Devices
        /// Use for the items of the combo box
        /// </summary>
        private CorsairDeviceType[] _availableDevices;
        public CorsairDeviceType[] AvailableDevices
        {
            get { return _availableDevices; }
        }

        /// <summary>
        /// The selected index of the combo box.
        /// </summary>
        private CorsairDeviceType _selectedDevice;
        public CorsairDeviceType SelectedDevice
        {
            get { return _selectedDevice; }
            set
            {
                if (value != _selectedDevice)
                {
                    _selectedDevice = value;
                    // Connect to the device
                    ((CUEDeviceBoard)_device).ConnectToDevice(value);
                    if (_device.Connected)
                    {
                        ConnectionStatus = "Device Connected";
                        EnableSelectLightingMode = true;
                        // Set the page to static lighting mode to allow the user to change the
                        // lighting mode now that we are connected to the CUE Device
                        SelectedLightingMode = new StaticViewModel(_device);
                        OnPropertyChanged(nameof(SelectedLightingMode));
                    }
                    else
                    {
                        ConnectionStatus = "Failed to connect to device";
                        EnableSelectLightingMode = false;
                    }
                    OnPropertyChanged(nameof(ConnectionStatus));
                    OnPropertyChanged(nameof(EnableSelectLightingMode));
                }
            }
        }

        /// <summary>
        /// Creates a tab for a CUE device.
        /// </summary>
        /// <param name="name">The name of the tab header.</param>
        public CueDeviceTab(string name)
        {
            TabType = tabType.CUE;
            Name = name;
            ConnectionStatus = "Device Disconnected";

            // Create the device object
            _device = new CUEDeviceBoard();

            // Get a list of the available cue devices
            _availableDevices = ((CUEDeviceBoard)_device).AvailableDevices;

            // This is a temporary viewmodel that is used before the user has connected to a device
            SelectedLightingMode = new InformationViewModel();
        }

    }
}
