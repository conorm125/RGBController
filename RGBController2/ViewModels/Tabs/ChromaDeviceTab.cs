﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using RGBController2.Boards;
using System.ComponentModel;
using RGBController2.ViewModels.LightingModes;

namespace RGBController2.ViewModels.Tabs
{
    /// <summary>
    /// The view model for an ChromaDeviceTabViewModel.
    /// </summary>
    public class ChromaDeviceTab : Tab, INotifyPropertyChanged
    {
        private ChromaDeviceBoard.DeviceTypes[] _availableDevices;
        public ChromaDeviceBoard.DeviceTypes[] AvailableDevices
        {
            get { return _availableDevices; }
        }

        private ChromaDeviceBoard.DeviceTypes _selectedDevice;
        public ChromaDeviceBoard.DeviceTypes SelectedDevice
        {
            get { return _selectedDevice; }
            set 
            {
                if (value != _selectedDevice)
                {
                    _selectedDevice = value;
                    EnableSelectLightingMode = true;
                    OnPropertyChanged(nameof(EnableSelectLightingMode));
                    _device = new ChromaDeviceBoard(value);
                    SelectedLightingMode = new StaticViewModel(_device);
                    OnPropertyChanged(nameof(SelectedLightingMode));
                }
            }
        }


        /// <summary>
        /// Construcats a ChromaDeviecTabViewModel.
        /// </summary>
        /// <param name="name">the name of the tab</param>
        public ChromaDeviceTab(string name)
        {
            TabType = tabType.Chroma;
            Name = name;
            ConnectionStatus = "Device Disconnected";

            // This is a temporary viewmodel that is used before the user has connected to a device
            SelectedLightingMode = new InformationViewModel();
            OnPropertyChanged(nameof(SelectedLightingMode));

            // Set the availble devices in the GUI
            // For now we have no method of finding which chroma devices are connected
            // So we will give the user a choice of all of them
            _availableDevices = new ChromaDeviceBoard.DeviceTypes[] {ChromaDeviceBoard.DeviceTypes.Headset, ChromaDeviceBoard.DeviceTypes.Keyboard, ChromaDeviceBoard.DeviceTypes.Keypad,
                ChromaDeviceBoard.DeviceTypes.Mouse, ChromaDeviceBoard.DeviceTypes.Mousepad};
            OnPropertyChanged(nameof(AvailableDevices));
        }

    }
}
