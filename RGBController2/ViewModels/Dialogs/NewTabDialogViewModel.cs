﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using RGBController2.Commands;
using RGBController2.Views.Dialogs;

namespace RGBController2.ViewModels.Dialogs
{
    /// <summary>
    /// NewTabDialogViewModel is the view model for the NewTabDialog.
    /// </summary>
    class NewTabDialogViewModel : BaseViewModel
    {
        /// <summary>
        /// The NewTabDialogView
        /// </summary>
        private NewTabDialogView DialogWindow;

        /// <summary>
        /// The command used for when the user presses the add tab button.
        /// </summary>
        public ICommand AddTabCommand { get; }

        /// <summary>
        /// The device name the user has eneter on the GUI.
        /// </summary>
        private string _deviceName;
        public string DeviceName
        {
            get { return _deviceName; }
            set { _deviceName = value; }
        }

        /// <summary>
        /// The device type the user has selected in the GUI.
        /// </summary>
        private int _selectedDeviceType;
        public int SelectedDeviceType
        {
            get { return _selectedDeviceType; }
            set { _selectedDeviceType = value; }
        }

        /// <summary>
        /// True if the user enetered parameters are valid, else false.
        /// e.g. this will be false if the user did not select a name for the device.
        /// </summary>
        private bool _success;
        public bool Success
        {
            get { return _success; }
        }

        /// <summary>
        /// Creates a NewTabDialogViewModel used for launching the NetTabView.
        /// </summary>
        public NewTabDialogViewModel()
        {
            AddTabCommand = new ActionCommand(p => AddTab());
            _success = false;
        }

        /// <summary>
        /// Creates window instance for this dialog viewmodel and displays it, getting the dialog result.
        /// </summary>
        public void ShowDialogWindow()
        {
            // This is a property of the DialogViewModelBase class - thus, each DialogViewModel holds a reference to its own DialogWindow:
            this.DialogWindow = new NewTabDialogView();
            // Tell the DialogWindow to display this ViewModel:
            this.DialogWindow.DataContext = this;
            // Launch the Window, using a method of the Window baseclass, that only returns when the window is closed:
            this.DialogWindow.ShowDialog();
        }

        /// <summary>
        /// This is the call back function used for the add button.
        /// </summary>
        public void AddTab()
        {
            // Do some validation here
            string errorMessage = "";

            if (DeviceName == "" || DeviceName == null)
                errorMessage += " - Device Name cannot be left empty\n";

            // Close the dialog window if validdation passes
            if (errorMessage == "")
            {
                _success = true;
                DialogWindow.Close();
            }
            else
                MessageBox.Show("Validation error\n" + errorMessage);
        }
    }
}
